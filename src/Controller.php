<?php

namespace Itgro\TokenActions;

use Illuminate\Routing\Controller as BaseController;
use Itgro\TokenActions\Models\TokenAction;

class Controller extends BaseController
{
    public function execute(string $token)
    {
        $action = TokenAction::byToken($token);

        if (!$action) {
            $redirectParams = TokenAction::getDefaultRedirectParams();

            $route = array_get($redirectParams, 'route');
            $parameters = array_get($redirectParams, 'parameters');
            $anchor = array_get($redirectParams, 'anchor');

            return redirect()->route($route, $parameters) . ($anchor ? '#' . $anchor : '');
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        $action->execute();

        return $action->getRedirect();
    }
}
