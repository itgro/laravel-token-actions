<?php

namespace Itgro\TokenActions\Models;

/**
 * @property string route
 * @property array parameters
 * @property string anchor
 */
class Redirect extends TokenAction
{
    public const TYPE = 'redirect';
    protected static $expiresInDays = 100;
    protected static $canBeExecutedOnlyOnce = false;

    /** @noinspection PhpDocSignatureInspection
     * @param $user
     * @param string $routeName
     * @param array $routeParameters
     * @param string $anchor
     *
     * @return Redirect
     */
    public static function makeFor($user): TokenAction
    {
        // Чтобы узнать зачем костыль, смотри родительский метод
        $args = func_get_args();

        $redirectParams = TokenAction::getDefaultRedirectParams();

        $route = array_get($args, 1, array_get($redirectParams, 'route'));
        $parameters = array_get($args, 2, array_get($redirectParams, 'parameters'));
        $anchor = array_get($args, 3, array_get($redirectParams, 'anchor'));

        return parent::makeFor($user, compact('route', 'parameters', 'anchor'));
    }

    public function getRouteAttribute()
    {
        return array_get($this->options, 'route', array_get(TokenAction::getDefaultRedirectParams(), 'route'));
    }

    public function setRouteAttribute($value)
    {
        $this->options['route'] = $value;
    }

    public function getParametersAttribute()
    {
        return array_get($this->options, 'parameters', array_get(TokenAction::getDefaultRedirectParams(), 'parameters'));
    }

    public function setParametersAttribute(array $value)
    {
        $this->options['parameters'] = $value;
    }

    public function getAnchorAttribute(): string
    {
        return array_get($this->options, 'anchor', array_get(TokenAction::getDefaultRedirectParams(), 'anchor'));
    }

    public function setAnchorAttribute($value): void
    {
        $this->options['anchor'] = $value;
    }

    protected function getRouteName(): string
    {
        return $this->route;
    }

    protected function getRouteParameters(): array
    {
        return $this->parameters;
    }

    protected function getRouteAnchor(): string
    {
        return $this->anchor;
    }
}
