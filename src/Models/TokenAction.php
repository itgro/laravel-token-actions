<?php

namespace Itgro\TokenActions\Models;

use Carbon\Carbon;
use Eloquent;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\Bus\Dispatcher as JobsDispatcher;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Events\Dispatcher;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Str;
use Itgro\TokenActions\Events\Failed;
use Itgro\TokenActions\Events\IsExecuting;
use Itgro\TokenActions\Events\WasExecuted;
use Itgro\TokenActions\Exceptions\AlreadyExecuted;
use Itgro\TokenActions\Exceptions\Expired;
use Itgro\TokenActions\Exceptions\InvalidType;
use Itgro\TokenActions\Exceptions\UserMismatch;

/**
 * @property string type
 * @property int user_id
 * @property string token
 * @property array options
 * @property bool is_first_execution
 * @property Carbon|null expires_at
 * @property Carbon|null executed_at
 * @property Carbon|null created_at
 * @property Carbon|null updated_at
 * @mixin \Eloquent
 */
class TokenAction extends Model
{
    public const TYPE = 'generic';
    protected static $expiresInDays = 7;
    protected static $canBeExecutedOnlyOnce = true;

    protected static $typeToClassMap = [];

    public $incrementing = false;
    // Нужно явно указать название таблицы, чтобы потомки класса пытались сохраниться туда, куда нужно
    protected $table = 'token_actions';
    protected $primaryKey = 'token';

    protected $casts = [
        'user_id' => 'int',
        'options' => 'array',
        'expires_at' => 'datetime',
        'executed_at' => 'datetime',
    ];

    /** @var StatefulGuard */
    protected $auth;

    /** @var Dispatcher */
    protected $eventsDispatcher;

    /** @var JobsDispatcher */
    protected $jobs;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->auth = app(Guard::class);
        $this->eventsDispatcher = app('events');
        $this->jobs = app(JobsDispatcher::class);
    }

    /**
     * @param array|string $actions
     * @throws InvalidType
     */
    public static function register($actions): void
    {
        foreach (array_wrap($actions) as $class) {
            if (static::isValidClass($class)) {
                throw new InvalidType($class);
            }

            static::$typeToClassMap[constant($class . '::TYPE')] = $class;
        }
    }

    public static function makeFor($user): self
    {
        /** @var self $model */
        $model = new static;

        // Поскольку strict standard не позволяет переопределённым в дочерних классах методам иметь другую сигнатуру,
        // приходится извращаться
        if (func_num_args() >= 2) {
            $options = (array)func_get_arg(1);
        } else {
            $options = [];
        }

        $model->type = static::TYPE;
        $model->token = static::generateToken();
        $model->expires_at = (new Carbon())->addDays(static::$expiresInDays);
        $model->options = $options;

        $model->user()->associate($user);

        $model->save();

        return $model;
    }

    public static function byToken(string $token): self
    {
        return self::where('token', $token)->first();
    }

    public static function getDefaultRedirectParams(): array
    {
        $route = config('token-actions.default_redirect.route', 'home');
        $parameters = config('token-actions.default_redirect.parameters', []);
        $anchor = config('token-actions.default_redirect.anchor', '');

        return compact('route', 'params', 'anchor');
    }

    protected static function generateToken(): string
    {
        return sprintf('%s-%s', time(), Str::random(32));
    }

    protected static function isValidClass($class): bool
    {
        return (
            $class &&
            class_exists($class) &&
            $class instanceof TokenAction &&
            defined($class . '::TYPE') &&
            constant($class . '::TYPE')
        );
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(config('token-actions.models.user'));
    }

    public function execute(): void
    {
        try {
            $this->checkThatCanBeExecuted();
            $this->checkIfExpired();
            $this->logUserIn();

            $this->eventsDispatcher->dispatch(new IsExecuting($this));

            $this->executed_at = Carbon::now();
            $this->save();

            $job = $this->getJob();

            if ($job) {
                $this->jobs->dispatch($job);
            }

            $this->eventsDispatcher->dispatch(new WasExecuted($this));
        } catch (AlreadyExecuted|Expired|UserMismatch $exception) {
            $this->eventsDispatcher->dispatch(new Failed($this, get_class($exception)));

            /** @noinspection PhpUnhandledExceptionInspection */
            throw $exception;
        }
    }

    /**
     * @return RedirectResponse|Redirector
     */
    public function getRedirect()
    {
        return redirect()->route($this->getRouteName(), $this->getRouteParameters()) . $this->getRouteAnchor();
    }

    /**
     * Create a new model instance that is existing.
     *
     * @param  \stdClass|array $attributes
     * @param  string|null $connection
     *
     * @return static
     * @throws InvalidType
     */
    public function newFromBuilder($attributes = [], $connection = null): self
    {
        $model = $this->makeModel($attributes->type);

        $model->exists = true;

        $model->setRawAttributes((array)$attributes, true);

        $model->setConnection($connection ?: $this->connection);

        return $model;
    }

    public function getIsFirstExecutionAttribute(): bool
    {
        return !$this->executed_at;
    }

    protected function getJob(): ?Dispatchable
    {
        return null;
    }

    protected function getRouteName(): string
    {
        return array_get(static::getDefaultRedirectParams(), 'route');
    }

    protected function getRouteParameters(): array
    {
        return array_get(static::getDefaultRedirectParams(), 'parameters');
    }

    protected function getRouteAnchor(): string
    {
        return array_get(static::getDefaultRedirectParams(), 'anchor');
    }

    /**
     * @throws AlreadyExecuted
     */
    protected function checkThatCanBeExecuted(): void
    {
        if (!$this->canBeExecuted()) {
            throw new AlreadyExecuted;
        }
    }

    protected function canBeExecuted(): bool
    {
        return $this->is_first_execution || !static::$canBeExecutedOnlyOnce;
    }

    /**
     * @throws Expired
     */
    protected function checkIfExpired(): void
    {
        if ($this->isExpired()) {
            throw new Expired;
        }
    }

    protected function isExpired(): bool
    {
        return is_a($this->expires_at, Carbon::class) && $this->expires_at->isPast();
    }

    /**
     * @throws UserMismatch
     */
    protected function logUserIn(): void
    {
        $this->checkIfUserCanBeLoggedIn();

        $this->auth->loginUsingId($this->user_id);
    }

    /**
     * @throws UserMismatch
     */
    protected function checkIfUserCanBeLoggedIn(): void
    {
        $user = $this->auth->user();

        if ($user && !$user->id === $this->user_id) {
            throw new UserMismatch;
        }
    }

    /**
     * @param string $type
     * @return static
     * @throws InvalidType
     */
    protected function makeModel(string $type): self
    {
        $class = array_get(static::$typeToClassMap, $type, '');

        if (static::isValidClass($class)) {
            throw new InvalidType($type);
        }

        return new $class;
    }
}
