<?php

namespace Itgro\TokenActions\Events;

use Itgro\TokenActions\Models\TokenAction;

class Failed
{
    public $action;
    public $reason;

    public function __construct(TokenAction $action, string $reason = '')
    {
        $this->action = $action;
        $this->reason = $reason;
    }
}
