<?php

namespace Itgro\TokenActions\Events;

use Itgro\TokenActions\Models\TokenAction;

class IsExecuting
{
    public $action;

    public function __construct(TokenAction $action)
    {
        $this->action = $action;
    }
}
