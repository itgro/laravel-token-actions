<?php

namespace Itgro\TokenActions\Exceptions;

use Exception;

class AlreadyExecuted extends Exception
{
}
