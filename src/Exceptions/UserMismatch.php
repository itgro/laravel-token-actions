<?php

namespace Itgro\TokenActions\Exceptions;

use Exception;

class UserMismatch extends Exception
{
}
