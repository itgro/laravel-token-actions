<?php

namespace Itgro\TokenActions\Exceptions;

use Exception;

class InvalidType extends Exception
{
    /**
     * @param string $code
     */
    public function __construct($code)
    {
        $this->message = sprintf('Unknown token action type "%s"', $code);
    }
}
