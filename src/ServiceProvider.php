<?php

namespace Itgro\TokenActions;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Itgro\TokenActions\Models\TokenAction;

class ServiceProvider extends BaseServiceProvider
{
    protected $defer = false;

    public function boot()
    {
        $this->publishConfig();
        $this->publishMigrations();
    }

    public function register()
    {
        $this->registerConfig();
        $this->registerActions();
        $this->registerRoutes();
        $this->registerHelpers();
    }

    protected function publishConfig()
    {
        $this->publishes([
            __DIR__ . '/../config/token-actions.php' => config_path('token-actions.php'),
        ], 'config');
    }

    protected function publishMigrations()
    {
        if (class_exists('CreateTokenActionsTable')) {
            return;
        }

        $timestamp = date('Y_m_d_His', time());

        $this->publishes([
            __DIR__ . '/../database/migrations/create_token_actions_table.php' => $this->app->databasePath() . "/migrations/{$timestamp}_create_token_actions_table.php",
        ], 'migrations');
    }

    protected function registerConfig()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/token-actions.php',
            'token-actions'
        );
    }

    protected function registerActions()
    {
        TokenAction::register(config('token-actions.actions'));
    }

    protected function registerRoutes()
    {
        require_once __DIR__ . '/../routes/web.php';
        $this->app->make(Controller::class);
    }

    protected function registerHelpers()
    {
        require_once __DIR__ . '/helpers.php';
    }
}
