<?php

use Itgro\TokenActions\Models\TokenAction;

if (!function_exists('link_to_token_action')) {
    function link_to_token_action(TokenAction $action, ?string $title = null, $parameters = [], $attributes = []): string
    {
        $parameters['token'] = $action->token;

        return link_to_route('token_actions::execute', $title, $parameters, $attributes);
    }
}
