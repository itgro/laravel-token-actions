<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTokenActionsTable extends Migration
{
    protected $table;
    protected $usersTable;
    protected $usersKey;

    public function __construct()
    {
        $this->table = config('token-actions.table');

        $userClass = config('token-actions.models.user');
        /** @var Eloquent $userModel */
        $userModel = new $userClass;

        $this->usersTable = $userModel->getTable();
        $this->usersKey = $userModel->getKeyName();
    }

    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->string('type');
            $table->unsignedInteger('user_id');
            $table->string('token')->primary();
            $table->json('options')->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->timestamp('executed_at')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references($this->usersKey)
                ->on($this->usersTable)
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
