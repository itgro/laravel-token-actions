<?php

Route::get('/token_action/{token}', 'Itgro\TokenActions\Controller@execute')
    ->name('token_actions::execute')
    ->middleware(['web']);
