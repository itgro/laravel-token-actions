<?php

use Itgro\TokenActions\Models\Redirect;

return [

    'table' => 'token_actions',

    'models' => [
        'user' => 'App\User',
    ],

    'actions' => [
        Redirect::TYPE => Redirect::class,
    ],

    'default_redirect' => [
        'route' => 'home',
        'parameters' => [],
        'anchor' => '',
    ],

];
